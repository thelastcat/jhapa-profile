import HighchartsVue from "highcharts-vue";
import Highcharts from "highcharts";
import highchartsMore from "highcharts/highcharts-more";
import highchartsVariablepie from "highcharts/modules/variable-pie";
import highchartsExporting from "highcharts/modules/exporting";
import highchartsExportdata from "highcharts/modules/export-data";
import highchartsOfflineexporting from "highcharts/modules/data";
import highchartsDrilldown from "highcharts/modules/drilldown";
highchartsMore(Highcharts);
highchartsVariablepie(Highcharts);
highchartsExporting(Highcharts);
highchartsExportdata(Highcharts);
highchartsOfflineexporting(Highcharts);
highchartsDrilldown(Highcharts);

Vue.use(HighchartsVue);
Vue.use(Highcharts);
