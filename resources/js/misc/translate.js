export default function translate(text, language) {
    if (!text) return; // uninitialized reports
    const translation = translation_obj.find(tr => {
        let texts = tr.text.split("|");
        return texts.find(t => {
            return (
                t && text && t.trim().toLowerCase() == text.trim().toLowerCase()
            );
        });
    });
    if (!translation) {
        console.log(`Couldn't find translation for: ${text}`);
        return text;
    }
    const translated_text = translation.translations.find(
        tr => tr.language === language
    );
    if (!translated_text) {
        console.log(`Couldn't translate "${text}" to "${language}"`);
        return text;
    }
    return translated_text.text;
}

const translation_obj = [
    {
        text: "Reports",
        translations: [{ text: "रिपोर्टहरू", language: "Nepali" }]
    },
    {
        text: "Population by gender",
        translations: [{ text: "लिंगको आधारमा जनसंख्या", language: "Nepali" }]
    },
    {
        text: "Population by age",
        translations: [{ text: "उमेरको आधारमा जनसंख्या", language: "Nepali" }]
    },
    {
        text: "Absent population",
        translations: [{ text: "अनुपस्थित जनसंख्या", language: "Nepali" }]
    },
    {
        text: "Educational qualification ",
        translations: [{ text: "शैक्षिक योग्यता", language: "Nepali" }]
    },
    {
        text:
            "Financially active population (10 years+)|Financially active population",
        translations: [
            {
                text: "आर्थिक रुपमा सक्रिय जनसंख्या",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Birth registration",
        translations: [{ text: "जन्मदर्ता", language: "Nepali" }]
    },
    {
        text: "Number of household by mother tongue|household by mother tongue",
        translations: [{ text: "मातृभाषाको आधारमा घरधुरी", language: "Nepali" }]
    },
    {
        text: "Population by mother tongue",
        translations: [
            { text: "मातृभाषाको आधारमा जनसंख्या", language: "Nepali" }
        ]
    },
    {
        text: "Number of household by religion|household by religion",
        translations: [{ text: "धर्मको आधारमा घरधुरी", language: "Nepali" }]
    },
    {
        text: "Population by religion",
        translations: [{ text: "धर्मको आधारमा जनसंख्या", language: "Nepali" }]
    },
    {
        text: "Number of household by ethnicity|household by ethnicity",
        translations: [
            { text: "जातजातीयताको आधारमा घरधुरी", language: "Nepali" }
        ]
    },
    {
        text: "Population by ethnicity",
        translations: [
            { text: "जातजातीयताको आधारमा जनसंख्या", language: "Nepali" }
        ]
    },
    {
        text: "Population by disability|Disability",
        translations: [
            { text: "अपाङ्गताको आधारमा जनसंख्या", language: "Nepali" }
        ]
    },
    {
        text: "Types of disability",
        translations: [{ text: "अपाङ्गताको प्रकार", language: "Nepali" }]
    },
    {
        text: "Number of household by occupation",
        translations: [{ text: "पेशाको आधारमा घरधुरी", language: "Nepali" }]
    },
    {
        text: "Status of people living abroad|abroad job",
        translations: [{ text: "विदेशमा बस्ने जनसंख्या", language: "Nepali" }]
    },
    {
        text: "Types of family|Family type",
        translations: [{ text: "परिवारको प्रकार", language: "Nepali" }]
    },
    {
        text: "Availability of toilet|toilet availability",
        translations: [{ text: "शौचालय उपलब्धता", language: "Nepali" }]
    },
    {
        text: "Household head by gender|house head gender",
        translations: [{ text: "लिंगको आधारमा घरधुरी", language: "Nepali" }]
    },
    {
        text: "Household by ownership of house|house ownership",
        translations: [
            { text: "स्वामित्वको आधारमा घरधुरी", language: "Nepali" }
        ]
    },
    {
        text: "Crops' types|crop types",
        translations: [{ text: "बालिनालीको प्रकार", language: "Nepali" }]
    },
    {
        text: "Source of irrigation",
        translations: [{ text: "सिँचाईको स्रोत", language: "Nepali" }]
    },
    {
        text: "Livestock in household|livestock",
        translations: [{ text: "पशुपालनको आधारमा घरधुरी", language: "Nepali" }]
    },
    {
        text: "Foodcrop's sufficiency",
        translations: [
            {
                text: "खाद्यान्न आत्मर्निभरताको अवस्था",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Production of crops (in kg)",
        translations: [
            {
                text: "बाली उत्पादन परिमाण (के.जी.)",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Production of foodcrop (in kg)",
        translations: [
            {
                text: "अन्न बाली उत्पादन परिमाण (के.जी.)",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Production of cashcrop (in kg)",
        translations: [
            {
                text: "नगदेबाली उत्पादन परिमाण (के.जी.)",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Production of vegetable (in kg)",
        translations: [
            {
                text: "तरकारी उत्पादन परिमाण (के.जी.)",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Production of fruit (in kg)",
        translations: [
            {
                text: "फलफुल उत्पादन परिमाण (के.जी.)",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Main source of light|source of light",
        translations: [{ text: "बत्तिको मुख्य स्रोत", language: "Nepali" }]
    },
    {
        text: "Alternative source of light",
        translations: [{ text: "बत्तिको बैकल्पिक स्रोत ", language: "Nepali" }]
    },
    {
        text: "Frequently used fuels",
        translations: [{ text: "इन्धन प्रयोगका आधारमा", language: "Nepali" }]
    },
    {
        text: "Electronic items",
        translations: [{ text: "उपलब्ध सेवा सुविधाहरु", language: "Nepali" }]
    },
    {
        text: "House storey",
        translations: [{ text: "घरको तला", language: "Nepali" }]
    },
    {
        text: "Type of house|house type",
        translations: [{ text: "घरको प्रकार", language: "Nepali" }]
    },
    {
        text: "Bank loan|Bank loan by household",
        translations: [{ text: "बैंक कर्जा", language: "Nepali" }]
    },
    {
        text: "Bank account|Bank account by household",
        translations: [{ text: "बैंक खाता", language: "Nepali" }]
    },
    {
        text: "Loan purpose|Bank loan purpose",
        translations: [{ text: "कर्जाको उद्देश्य", language: "Nepali" }]
    },
    {
        text: "Participation in organization",
        translations: [{ text: "संस्थामा आवद्ध", language: "Nepali" }]
    },
    {
        text: "Total annual remittance (in Rs.)",
        translations: [
            { text: "वैदेशिक आय स्रोत (वार्षिक)", language: "Nepali" }
        ]
    },
    {
        text: "Countries of foreign employment",
        translations: [
            {
                text: "वैदेशिक रोजगारका लागि गन्तव्य देश",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Household having balanced diet|Balanced diet",
        translations: [{ text: "सन्तुलित खाना", language: "Nepali" }]
    },
    {
        text: "Household health insurance|Health insurance",
        translations: [{ text: "स्वास्थ्य बिमा", language: "Nepali" }]
    },
    {
        text: "Unexpected deaths|Unexpected death",
        translations: [{ text: "असामयिक निधन", language: "Nepali" }]
    },
    {
        text:
            "Awareness of nutrition during pregnancy|Nutrition during pregnancy",
        translations: [
            {
                text: "गर्भवति महिला लाई पोषण सम्बन्धि जानकारी",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Child birth location",
        translations: [{ text: "बच्चा जन्मेको स्थान", language: "Nepali" }]
    },
    {
        text: "Use of vaccine|Child vaccination",
        translations: [{ text: "खोपको प्रयोग", language: "Nepali" }]
    },
    {
        text: "BMI of children",
        translations: [{ text: "बच्चाको नापतौल", language: "Nepali" }]
    },
    {
        text: "Duration of breast-feeding",
        translations: [{ text: "स्तनपानको समयावधी", language: "Nepali" }]
    },
    {
        text: "Household affected by natural disaster|Natural disaster",
        translations: [
            {
                text: "प्राकृतीक प्रकोपले प्रभावित घरधुरी",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Demography",
        translations: [
            {
                text: "जनसांख्यिकी",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Housing and amenities",
        translations: [
            {
                text: "आवास र सुविधाहरु",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Finance",
        translations: [
            {
                text: "वित्त",
                language: "Nepali"
            }
        ]
    },
    {
        text: "Health",
        translations: [
            {
                text: "स्वास्थ्य",
                language: "Nepali"
            }
        ]
    }
];
