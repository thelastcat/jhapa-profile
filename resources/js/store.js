import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
// {
//     title: "Population by gender",
//     name: "Gender",
//     column: "gender",
//     personal: true
// },

// {
//     title: "Birth registration",
//     name: "Birth registration",
//     column: "birthregistration",
//     personal: true
// }
export const store = new Vuex.Store({
    state: {
        report_layout: [
            {
                name: "Demography",
                value: "demography",
                sub_category: [
                    {
                        title: "Household by religion",
                        name: "Religion",
                        column: "family_religion"
                    },
                    {
                        title: "Population by religion",
                        name: "Religion",
                        column: "religion",
                        personal: true
                    },
                    {
                        title: "Household by mother tongue",
                        name: "Mother tongue",
                        column: "family_mothertongue"
                    },
                    {
                        title: "Population by mother tongue",
                        name: "Mother tongue",
                        column: "mothertongue",
                        personal: true
                    },
                    {
                        title: "Household by ethnicity",
                        name: "Ethnicity",
                        column: "family_ethnicity"
                    },
                    {
                        title: "Population by ethnicity",
                        name: "Ethnicity",
                        column: "ethnicity",
                        personal: true
                    },
                    {
                        title: "Educational qualification",
                        name: "Educational qualification",
                        column: "educational_qualification",
                        personal: true
                    },
                    // {
                    //     title: "Birth registration",
                    //     name: "Birth registration",
                    //     column: "birthregistration",
                    //     personal: true,
                    //     map: { Yes: "Registered", No: "Not registered" }
                    // },
                    {
                        title: "Family type",
                        column: "family_type",
                        map: { single: "Single family", joint: "Joint family" }
                    },
                    {
                        title: "Availability of toilet",
                        name: "Toilet availability",
                        column: "toilet",
                        map: {
                            Yes: "Toilet available",
                            No: "Toilet unavailable"
                        }
                    },
                    {
                        title: "House ownership",
                        column: "house_ownership"
                    }
                ]
            },

            {
                name: "Housing and amenities",
                value: "housing",

                sub_category: [
                    {
                        title: "Main source of light",
                        name: "Light source",
                        column: "light_source"
                    },
                    {
                        title: "Electronic items",
                        column: "item_electronic",
                        multiple: true
                    },
                    {
                        title: "Frequently used fuels",
                        name: "Fuel",
                        column: "fuel_frequent"
                    },
                    {
                        title: "Type of house",
                        name: "House type",
                        column: "house_type"
                    }
                ]
            },
            {
                name: "Health",
                value: "health",
                sub_category: [
                    {
                        title: "Use of vaccine",
                        column: "child_vaccine",
                        personal: true,
                        map: {
                            Yes: "Vaccinated children",
                            No: "Unvaccinated children"
                        }
                    },
                    {
                        title: "Household health insurance",
                        column: "health_insurance"
                    },
                    // {
                    //     title: "BMI of children",
                    //     column: "child_bmi",
                    //     personal: true,
                    //     map: {
                    //         Yes: "Normal",
                    //         No: "Below normal",
                    //         DonotKnow: "Unknown"
                    //     }
                    // },
                    {
                        title: "Natural disaster",
                        column: "natural_disaster",
                        multiple: true
                    }
                ]
            },
            {
                name: "Finance",
                value: "finance",
                sub_category: [
                    {
                        title: "Bank loan by household",
                        name: "Bank loan",
                        column: "bank_loan",
                        map: { Yes: "Taken loan", No: "Hasn't taken loan" }
                    },
                    {
                        title: "Bank account by household",
                        column: "bankaccount",
                        map: {
                            Yes: "Has bank account",
                            No: "Doesn't have bank account"
                        }
                    },
                    {
                        title: "Countries of foreign employment",
                        column: "residing_country",
                        personal: true
                    }
                ]
            },
            {
                name: "GIS maps",
                value: "gis_map",
                type: "map",
                sub_category: [
                    {
                        type: "map",
                        title: "Slope map",
                        name: "slope_map.jpg"
                    },
                    {
                        type: "map",
                        title: "Road map (names)",
                        name: "road_map_names.jpg"
                    },
                    {
                        type: "map",
                        title: "Road map (classified)",
                        name: "road_map_classified.jpg"
                    },
                    {
                        type: "map",
                        title: "Administrative boundary",
                        name: "administrative boundary(new_wards).jpg"
                    },
                    {
                        type: "map",
                        title: "Contour map",
                        name: "contour_map.jpg"
                    },
                    {
                        type: "map",
                        title: "DEM",
                        name: "DEM.jpg"
                    },
                    {
                        type: "map",
                        title: "land_usage",
                        name: "land_use.jpg"
                    }
                ]
            }
        ],

        member_layout: [
            {
                name: "Name",
                value: "member_name",
                ignore: true,
                rules: [
                    v => !!v || "Name is required",
                    v =>
                        (v && v.length <= 200) ||
                        "Name must be less than 200 characters"
                ]
            },
            {
                name: "Age",
                value: "age",
                number: true,
                rules: [
                    v => !!v || "Age is required",
                    v => (v && v > 0 && v < 200) || "Invalid age"
                ]
            },
            {
                name: "Gender",
                value: "gender",
                select: true,
                fetch: true
            },
            {
                name: "Religion",
                value: "religion",
                select: true,
                fetch: true
            },
            {
                name: "Ethnicity",
                value: "ethnicity",
                select: true,
                fetch: true
            },
            {
                name: "Mother tongue",
                value: "mothertongue",
                select: true,
                fetch: true
            },
            {
                name: "Educational qualification",
                value: "educational_qualification",
                select: true,
                fetch: true
            },
            {
                name: "Maritial status",
                value: "marriage_status",
                select: true,
                fetch: true,
                linked: {
                    age: {
                        $gt: 18
                    }
                }
            },

            {
                name: "Father's name",
                value: "father_name"
            },
            {
                name: "Child vaccination",
                value: "child_vaccine",
                select: true,
                fetch: true,
                linked: {
                    age: {
                        $lt: 5
                    }
                }
            },

            // {
            //     name: "Financially active",
            //     value: "financial_active",
            //     select: true,
            //     fetch: true,
            //     linked: {
            //         age: {
            //             $gt: 15
            //         }
            //     }
            // },
            // {
            //     name: "Cause of financial inactiveness",
            //     value: "financial_cause_inactive",
            //     select: true,
            //     fetch: true,
            //     multiple: true,
            //     linked: {
            //         financial_active: "No"
            //     }
            // },

            {
                name: "Disability",
                value: "disability",
                select: true,
                fetch: true
            },
            {
                name: "Disability type",
                value: "disability_type",
                linked: {
                    disability: "Yes"
                },
                select: true,
                fetch: true
            }
        ],
        language: localStorage.getItem("language") || "English"
    },
    getters: {
        getDemographyLayout: state => {
            return state.report_layout.find(type => type.value == "demography")
                .sub_category;
        },
        getHousingLayout: state => {
            return state.report_layout.find(type => type.value == "housing")
                .sub_category;
        },
        getHealthLayout: state => {
            return state.report_layout.find(type => type.value == "health")
                .sub_category;
        },
        getFinanceLayout: state => {
            return state.report_layout.find(type => type.value == "finance")
                .sub_category;
        },
        getCurrentLanguage: state => {
            return state.language;
        }
    },

    mutations: {
        setLanguage(state, language) {
            localStorage.setItem("language", language);
            state.language = language;
        }
    }
});
