require("./bootstrap");

window.Vue = require("vue");
import camelCase from "lodash/camelCase";
import upperFirst from "lodash/upperFirst";
import * as VueGoogleMaps from "vue2-google-maps";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import Vuex from "vuex";
import { store } from "./store";

// import "@mdi/font/css/materialdesignicons.css";

require("./highcharts");

const vuetify = new Vuetify();

Vue.use(Vuex);
Vue.use(Vuetify);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyDZ9IWfIO7A4CBPI_qg3OMvXs3eSvjUj3s",
        libraries: "places" // necessary for places input
    }
});

const requireComponent = require.context(
    // The relative path of the components folder
    "./",
    // Whether or not to look in subfolders
    true,
    // The regular expression used to match base component filenames
    /\.vue$/i
);

requireComponent.keys().forEach(fileName => {
    const componentConfig = requireComponent(fileName);
    const componentName = upperFirst(
        camelCase(
            fileName
                .split("/")
                .pop()
                .replace(/\.\w+$/, "")
        )
    );

    Vue.component(componentName, componentConfig.default || componentConfig);
});

const app = new Vue({
    store,
    el: "#app",
    vuetify
});
