@extends('layouts.app')

@section('content')
<div class="container-fluid mx-1">
    <allcategories :project_ward="{{ $project_ward }}"
        :auth_user="{{ auth()->user()?'true':'false' }}"></allcategories>
</div>
@endsection

@section('homebtn') border-b font-semibold @endsection
