<nav class="header" style="z-index:10; position: relative;">
    <div class="header--left">
        <div class="flex items-center">
        <img src="{{asset('/resources/gov_logo.png')}}" width="60">
        <div class="ml-3">
        <a href="{{route('home')}}" class="text-xl">{{$project_name}}</a>
        <div class="border-solid border-l-2 border-white">Data and statistics</div>
        </div>
        </div>
    </div>
    <div class="header--right">
        <li><a href="{{route('home')}}"
                class="inline-block text-sm px-4 py-2 leading-none text-white hover:border-transparent hover:text-teal-500 hover:bg-white @yield('homebtn')">Home</a>
        </li>
        <li>
            <a href="{{route('institutional')}}" class="inline-block text-sm px-4 py-2 leading-none text-white hover:border-transparent hover:text-teal-500 hover:bg-white @yield('instbtn')">Institutional</a>
        </li>
        <li>
            <a href="{{route('reports')}}" class="inline-block text-sm px-4 py-2 leading-none text-white hover:border-transparent hover:text-teal-500 hover:bg-white @yield('reportbtn')">Reports</a>
        </li>
        @if(auth()->user())

        <li><a href="{{route('admin')}}"
                class="inline-block text-sm px-4 py-2 leading-none text-white hover:border-transparent hover:text-teal-500 hover:bg-white @yield('adminbtn')">Admin
                panel</a>
        </li>
        @endif
        <li>
            @if(auth()->user())
            <form action="logout" method="post">
                {{csrf_field()}}
                <button
                    class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white">
                    Logout
                </button>
            </form>
            @else
            <a href="./login"
                class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-teal-500 hover:bg-white">
                Login
            </a>
            @endif
        </li>

    </div>
</nav>
