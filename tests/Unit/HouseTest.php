<?php

namespace Tests\Unit;

use App\House;
use App\Http\Controllers\HouseController;
use Tests\TestCase;
use Illuminate\Support\Facades\App;

class HouseTest extends TestCase
{
    /** @test */
    public function has_more_than_one_house()
    {
        $houses = null;
        $this->assertTrue(House::get()->count() > 0);
        // dd($house);
    }

    public function has_column($model, $name)
    {
        return $model->getConnection()
            ->getSchemaBuilder()
            ->hasColumn($model->getTable(), $name);
    }
    /** @test */
    public function table_has_required_columns()
    {
        $house = new House;
        $this->assertTrue($this->has_column($house, "created_at"));
        $this->assertTrue($this->has_column($house, "updated_at"));
        $this->assertTrue($this->has_column($house, "house_ID"));
        // dd($test);
    }

    /** @test */
    public function can_get_reports()
    {
    }
}
