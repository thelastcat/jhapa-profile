<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/compare', 'HomeController@compare')->name("compare");
Route::get('/institutional', 'HomeController@institutional')->name("institutional");

Route::get('/reports', 'HomeController@reports')->name('reports');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::get('/admin/personal', 'AdminController@member_index');
Route::get('/admin/household/{household_id}', 'AdminController@show')->name('admin_household');

Route::post('/admin/json/member', 'AdminController@member_indexapi');


Route::get('/purge', 'AdminController@purge_cache');

// Mainly related to household
Route::post('/admin/json/household/{household_id}/save', 'AdminController@saveapi');
Route::get('/admin/json/household/{household_id}', 'AdminController@showapi');
Route::post('/admin/json', 'AdminController@indexapi')->name('adminapi');
Route::get('/admin/json/columns', 'AdminController@getAllColumns');
Route::post("/admin/json/household/count/{column_name}", "AdminController@count_house_value");
// Related to members
Route::get('/admin/json/member/{household_id}', 'AdminController@showapi_member');
Route::post('/admin/json/member/{household_id}/add', 'AdminController@addapi_member');
Route::get("/admin/json/member/count/{column_name}", "AdminController@count_member_value");
Route::post('/admin/json/member/{person_id}/save', 'AdminController@saveapi_member');
Route::post('/admin/json/member/{person_id}/delete', 'AdminController@deleteapi_member');
