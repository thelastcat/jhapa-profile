<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware('cache.headers:public;max_age=2628000')->group(function () {
    Route::get("/institutional", "InstitutionController@index");

    Route::post("/household/count/{column_name}", "HouseController@count_value");
    Route::post("/personal/count/{column_name}", "MemberController@count_value");

    Route::get("/household/report/{column_name}", "HouseController@report_value");
    Route::get("/personal/report/{column_name}", "MemberController@report_value");



    Route::post("/overview", "HouseController@overview");
// });
