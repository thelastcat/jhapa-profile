<?php

namespace App\Http\Controllers;

use App\House;
use App\Member;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function member_index()
    {
        return view("admin.member_index");
    }

    public function indexapi(Request $request)
    {
        if ($request->complete_data == true) {
            return House::get();
        }
        $wards = $request->wards;
        $columns = $request->columns;
        $custom_filters = $request->custom_filters;
        if (!is_array($columns)) {
            die();
        }
        $houses = House::select($columns)
            ->orderBy('ward_number', 'asc');
        if (count($wards)) {
            $houses = $houses->whereIn("ward_number", $wards);
        }

        if ($request->house_ID) {
            $houses = $houses->where('house_ID', 'like', '%' . $request->house_ID . '%');
        }

        if ($request->house_head) {
            $houses = $houses->where('house_head', 'like', '%' . $request->house_head . '%');
        }

        if (is_array($custom_filters)) {
            foreach ($custom_filters as $filter) {
                $houses = $houses->whereIn($filter['column'], $filter['values']);
            }
        }
        if ($request->no_paginate == true) {
            $houses = $houses->get();
        } else {
            $houses = $houses->paginate(100);
        }
        return $houses;
    }



    public function member_indexapi(Request $request)
    {
        if ($request->complete_data == true) {
            return Member::get();
        }
        $wards = $request->wards;
        $columns = $request->columns;
        $custom_filters = $request->custom_filters;
        if (!is_array($columns)) {
            die();
        }
        $members = Member::select($columns)
            ->orderBy('ward_number', 'asc');
        if (count($wards)) {
            $members = $members->whereIn("ward_number", $wards);
        }

        if ($request->house_ID) {
            $members = $members->where('house_ID', 'like', '%' . $request->house_ID . '%');
        }

        if ($request->member_name) {
            $members = $members->where('member_name', 'like', '%' . $request->member_name . '%');
        }

        if (is_array($custom_filters)) {
            foreach ($custom_filters as $filter) {
                $members = $members->whereIn($filter['column'], $filter['values']);
            }
        }
        if ($request->no_paginate == true) {
            $members = $members->get();
        } else {
            $members = $members->paginate(100);
        }
        return $members;
    }





    public function count_house_value(Request $request, $column_name)
    {
        $house = new House;
        $house = $house->select($column_name)->distinct()->get()->toArray();
        // $house = $house->get()->groupBy($column_name)->map(function ($val) {
        //     return $val->count();
        // });
        return $house;
    }

    public function show($house_ID)
    {
        $house = House::where("house_ID", $house_ID)->firstOrFail();
        return view("admin.show", compact("house_ID"));
    }

    public function showapi($house_ID)
    {
        $house = House::where("house_ID", $house_ID)->firstOrFail();
        return $house;
    }

    public function saveapi(Request $request, $house_ID)
    {
        $house = House::where("house_ID", $house_ID)->firstOrFail();
        $changes = $request->changes;

        foreach ($changes as $change) {
            $house[$change['column']] = $change['value'];
        }
        $house->update();
        return $house;
    }


    //house member related

    public function showapi_member($house_ID)
    {
        $members = Member::where("house_ID", $house_ID)->get();
        return $members;
    }

    public function count_member_value(Request $request, $column_name)
    {
        $member = new Member;
        $member = $member->select($column_name)->orderBy($column_name)->distinct()->get()->toArray();
        return $member;
    }

    public function addapi_member(Request $request, $house_ID)
    {
        $data = $request->data;
        $member = new Member;
        $member->person_ID = Str::random();
        $member->house_ID = $house_ID;
        foreach ($data as $input) {
            if (is_array($input['value'])) {
                $member[$input['column']] = implode(" ", $input['value']);
            } else {
                $member[$input['column']] = $input['value'];
            }
        }
        $member->save();
        return $member;
    }

    public function saveapi_member(Request $request, $person_ID)
    {
        $member = Member::where("person_ID", $person_ID)->firstOrFail();
        $changes = $request->changes;

        foreach ($changes as $change) {
            $member[$change['column']] = $change['value'];
        }
        $member->update();
        return $member;
    }

    public function deleteapi_member($person_ID)
    {
        $member = Member::where("person_ID", $person_ID)->firstOrFail();
        $member->delete();
        return;
    }


    public function purge_cache(){
        Cache::flush();
        return redirect()->route("home");
    }
}
