<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//         Cache::flush();
        // Cache::forever( "household", "test");
        // dd(Cache::has('householdz'));
        // dd(Cache::get('household'));
        return view('home');
    }

    public function reports()
    {
        return view('reports');
    }
    public function compare()
    {
        return view('compare');
    }
    public function institutional()
    {
        return view('institutional');
    }
}
