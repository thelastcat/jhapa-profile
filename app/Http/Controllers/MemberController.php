<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Member;
use Illuminate\Support\Facades\Cache;

class MemberController extends Controller
{
    public function count_value(Request $request, $column_name)
    {
        $wards = $request->wards;

        $ward_str = implode("-", $wards);
        $cache_name = "personal_" . $ward_str . "_" . $column_name;

        $member = new Member;

        $public_columns = $member->get_public_columns();
        if (!in_array($column_name, $public_columns)) {
            abort(401, "Unauthorized");
        }

        if(Cache::has($cache_name)){
            return Cache::get($cache_name);
        }

        if (count($wards)) {
            $member = $member->whereIn("ward_number", $wards);
        }
        $member = $member->get($column_name)->groupBy($column_name)->map(function ($val) {
            return $val->count();
        });
        Cache::put( $cache_name, $member->toJson(), 43200);
        return $member;
    }

    public function report_value($column_name)
    {
        $member = new Member;

        $public_columns = $member->get_public_columns();
        if (!in_array($column_name, $public_columns)) {
            abort(401, "Unauthorized");
        }

        $member = $member->get(["ward_number",$column_name])->groupBy("ward_number")->map(function ($val) use ($column_name) {
            return $val->groupBy($column_name)->map(function ($nestedval) {
                return $nestedval->count();
            });
        });
        return $member;
    }
}
