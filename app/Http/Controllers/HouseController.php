<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\House;
use App\Member;
use Illuminate\Support\Facades\Cache;

class HouseController extends Controller
{
    public function count_value(Request $request, $column_name)
    {
        $wards = $request->wards;

        $ward_str = implode("-", $wards);
        $cache_name = "household_" . $ward_str . "_" . $column_name;

        $house = new House;
        $public_columns = $house->get_public_columns();
        if (!in_array($column_name, $public_columns)) {
            abort(401, "Unauthorized");
        }
        if(Cache::has($cache_name)){
            return Cache::get($cache_name);
        }

        if (count($wards)) {
            $house = $house->whereIn("ward_number", $wards);
        }
        $house = $house->get($column_name)->groupBy($column_name)->map(function ($val) {
            return $val->count();
        });
        Cache::put( $cache_name, $house->toJson(), 43200);

        return $house;
    }

    public function overview(Request $request)
    {
        $wards = $request->wards;

        $ward_str = implode("-", $wards);
        $cache_name = "household_" . $ward_str . "_overview";
        if(Cache::has($cache_name)){
            return Cache::get($cache_name);
        }

        $overview =  [];
        $member = new Member;
        $house = new House;
        if (count($wards)) {
            $member = $member->whereIn("ward_number", $wards);
            $house = $house->whereIn("ward_number", $wards);
        }

        $population = $member->get(["gender", "age_group"])->groupBy("gender")->map(function ($val) {
            return $val->groupBy("age_group")->map(function ($kek) {
                return $kek->count();
            });
        });
        $overview = array(
            'total_household' => $house->count(),
            'population' => $population
        );
        Cache::put( $cache_name, $overview, 10080);
        return $overview;
    }

    public function report_value($column_name)
    {
        $house = new House;

        $public_columns = $house->get_public_columns();
        if (!in_array($column_name, $public_columns)) {
            abort(401, "Unauthorized");
        }

        $house = $house->get(["ward_number", $column_name])->groupBy("ward_number")->map(function ($val) use ($column_name) {
            return $val->groupBy($column_name)->map(function ($nestedval) {
                return $nestedval->count();
            });
        });
        return $house;
    }
}
