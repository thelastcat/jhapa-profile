<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('project_name', 'Jhapa Rural Municipality');
        View::share('project_ward', 7);
        Schema::defaultStringLength(191);
    }
}
