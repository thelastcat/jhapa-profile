<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $primaryKey = 'person_ID';

    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [];
    public function get_public_columns(){
        return [
            "ward_number",
            "age",
            "age_group",
            "gender",
            "ethnicity",
            "religion",
            "maritial_status",
            "mothertongue",
            "disability_type",
            "child_vaccine",
            "child_bmi",
            "educational_qualification",
            "residing_country",
            "occupation"
        ];
    }
}
