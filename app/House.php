<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    protected $primaryKey = 'house_ID';

    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [];
    public function get_public_columns()
    {
        return [
            "house_ownership",
            "house_owner_gender",
            "house_land_type",
            "house_type",
            "house__storey",
            "house_design",
            "family_type",
            "settlement_type",
            "family_ethnicity",
            "family_religion",
            "family_mothertongue",
            "family_language",
            "family_occupation",
            "family_migration",
            "toilet",
            "light_source",
            "fuel_frequent",
            "item_electronic",
            "bank_loan",
            "decisionmaking",
            "bankaccount",
            "health_insurance",
            "natural_disaster"
        ];
    }
}
